package net.synergyserver.synergylobby.listeners;

import net.synergyserver.synergycore.events.WorldGroupProfileCreateEvent;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 * Listens to various events emitted by <code>SynergyPlugin</code>s.
 */
public class SynergyListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onWorldGroupProfileCreate(WorldGroupProfileCreateEvent event) {
        WorldGroupProfile wgp = event.getNewWGP();

        // Ignore the event if the WGP isn't for this plugin
        if (!wgp.getWorldGroupName().equals(SynergyLobby.getWorldGroupName())) {
            return;
        }

        // Inject a LobbyworldGroupProfile into the event
        event.setNewWGP(new LobbyWorldGroupProfile(wgp));
    }

}
