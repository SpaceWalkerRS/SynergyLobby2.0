package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "thehunt",
        aliases = {"egghunt", "hunt"},
        permission = "syn.the-hunt",
        usage = "/thehunt <progress|leaderboard|reset>",
        description = "Main command for \"The Hunt\" in Lobby.",
        minArgs = 1
)
public class TheHuntCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
